def is_palindrome(inp_2):
    inp_2 = inp_2.lower()
    inp_2 = inp_2.replace(' ', '')
    # HOW THE ***HECK*** THIS IS BLACK MAGIC
    return inp_2 == inp_2[::-1]


def is_palindrome_one(inp_1):
    for i in range(len(inp_1)):
        inp_1 = inp_1.lower()
        inp_1 = inp_1.replace(' ', '')
        if inp_1[i] != inp_1[len(inp_1) - 1 - i]:
            return False
        # We could do this, but we could also do this instead
        '''if word[i] == word[len(word) - 1 -i]:
            return True'''
    # This is what I meant BTW
    return True


word = input('Enter a phrase:    ')
print(is_palindrome(word))
