def same_number_cat_dog(str):
    cat = 0
    dog = 0
    for i in range(len(str)):
        if str[i:i + 3] == 'cat':
            cat += 1
        elif str[i:i + 3] == 'dog':
            dog += 1
    return cat == dog


words = input('Enter some words:    ')
print(same_number_cat_dog(words))
