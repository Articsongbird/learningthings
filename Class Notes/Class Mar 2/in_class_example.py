def three_means(*numbers):
    # * is an unpacking operator
    ans1 = sum(numbers) / len(numbers)
    return ans1


# Test conditions
print(three_means(2))
print(three_means(4, 1))
print(three_means(2, 7, 3))
print(three_means(8, 3, 11, 55))
