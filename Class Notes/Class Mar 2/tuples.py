def min_max(t):
    return min(t), max(t)


def sum_all(*args):
    return sum(args)


print(sum_all(3, 2, 1))
