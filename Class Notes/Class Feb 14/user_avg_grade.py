"""
    Done in class Feb 14 2021
"""

# Gets user input and parses it as an integer
grade = int(input('Enter a grade or -1 to stop:   '))

total = 0
count = 0

while grade != -1:
    total = total + grade
    count = count + 1
    grade = int(input('Enter a grade or -1 to stop:   '))

if count > 0:
    average = total / count
    print(f'Your average grade is {average:.2f}.')
else:
    print('You did not enter any grades')
