'''

'''

n = 1

while n < 5:
    print('7 raised to the power {:d} is {:d}.'.format(n, 7 ** n))
    n = n + 1

print()
