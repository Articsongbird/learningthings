"""
            1
          2 1 2
        3 2 1 2 3
      4 3 2 1 2 3 4
    5 4 3 2 1 2 3 4 5
  6 5 4 3 2 1 2 3 4 5 6
7 6 5 4 3 2 1 2 3 4 5 6 7                               """

n = int(input('Enter number of lines:   '))

for i in range(1, n + 1):
    for k in range(n, 1, -1):
        if k > i:
            print(' ', end=' ')
        else:
            print(k, end=' ')
    for j in range(1, i + 1):
        print(j, end=' ')

    print()
