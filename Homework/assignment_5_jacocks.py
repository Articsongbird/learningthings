"""

    Project Notes:
    * if completed
    _ if uncompleted


    * User enters three integers
    * prog passes ints to function
    * func finds middle value
    * prog output

"""


def find_middle(inp_1, inp_2, inp_3):
    if (inp_3 < inp_1 < inp_2) or (inp_3 > inp_1 > inp_2):
        # In this result, inp_1 is the middle
        print(f'The middle value of {inp_1}, {inp_2}, and {inp_3} is: {inp_1}.')
    elif (inp_3 < inp_2 < inp_1) or (inp_3 > inp_2 > inp_1):
        # In this result, inp_2 is the middle
        print(f'The middle value of {inp_1}, {inp_2}, and {inp_3} is: {inp_2}.')
    elif (inp_2 < inp_3 < inp_1) or (inp_2 > inp_3 > inp_1):
        # In this result, inp_3 is the middle
        print(f'The middle value of {inp_1}, {inp_2}, and {inp_3} is: {inp_3}.')
    else:
        print("Error: Invalid input")


print("Assignment 5 by Christian A. Jacocks, DSC ID: 900101074")
print("Assignment Due: February 16, 2022\n")
print('\nPlease enter three numbers in following the prompt below')

inp1 = int(input('Please enter the first integer:    '))
inp2 = int(input('Please enter the second integer:   '))
inp3 = int(input('Please enter the third integer:    '))
print('\n')

find_middle(inp1, inp2, inp3)
print('\n')

input('Press any key to close')
