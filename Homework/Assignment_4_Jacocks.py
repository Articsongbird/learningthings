def calc_tax(tax_bracket_percent, base_income, tax_already_paid, tax_bracket_cutoff):
    # calc_tax(tax_percent, income, already_taxed, tax_cutoff)

    income_over_bracket_start = base_income - tax_bracket_cutoff
    ans = tax_already_paid + (income_over_bracket_start * tax_bracket_percent)
    print(f'The taxes on an income of: ${base_income:.2f} are : ${ans:.2f}.')


income = float(input('\nPlease enter your taxable income in USD: $'))

'''
tax_percent is the tax percent for the given bracket
tax_cutoff is the amount that is subracted from the reported income and
            changes based on bracket
already_taxed is the taxes already owed from the previous bracket.
'''
if (income >= 0 and income < 9526.0):
    tax_percent = .1
    already_taxed = 0
    tax_cutoff = 0
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 9526.0 and income < 38701):
    tax_percent = .15
    already_taxed = 952.5
    tax_cutoff = 9525
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 38701 and income < 93701):
    tax_percent = .25
    already_taxed = 5328.75
    tax_cutoff = 38700
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 93701 and income < 195451):
    tax_percent = .28
    already_taxed = 19078.75
    tax_cutoff = 937000
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 195451 and income < 424951):
    tax_percent = .33
    already_taxed = 47568.75
    tax_cutoff = 195450
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 424951 and income < 426701):
    tax_percent = .35
    already_taxed = 123303.75
    tax_cutoff = 424950
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
elif (income >= 426701):
    tax_percent = .396
    already_taxed = 123916.25
    tax_cutoff = 426700
    calc_tax(tax_percent, income, already_taxed, tax_cutoff)
else:
    print('\nInvalid input, income cannot be negative.')
