def detect_all_space(word):
    for i in range(len(word)):
        if not word[i].isspace():
            return False
    return True


def detect_last_space(word):
    word_last_char = len(word) - 1
    if not word[word_last_char].isspace():
        return False
    return True


def detect_leading_space(word):
    if not word[0].isspace():
        return False
    return True


loop_can_run = 1
it_count = 0
valid_it = 1
string_output = ''

print("Assignment 6 by Christian A. Jacocks, DSC ID: 900101074")
print("Assignment Due: February 23, 2022\n")
print('\nPlease enter words, characters, or numbers in following the prompt below.')
print('Valid input may be any symbols, letters, or numbers.')
print('Valid input may not have leading or trailing whitespace.')
print('Valid input may not consist only of whitespace.')
print("To finish entering data, type the keywords 'HALT' or 'halt' at any point.")
print('\n\n')

while loop_can_run == 1:
    it_count += 1
    user_input = input('Enter a word or enter "HALT" to halt the program:    ')

    if it_count >= 300:
        print('WARNING: EXITING DUE TO EXCESSIVE USE')
        loop_can_run = 0
    elif it_count >= 30:
        print('WARNING: This will run perpetually until the exit keywords, "halt" or "HALT" are entered.')

    if user_input == 'HALT' or user_input == 'halt':
        print('*** Input gathering halted!!!!')
        loop_can_run = 0
    elif len(user_input) == 0:
        print('ERROR: Nothing Entered')
    elif detect_leading_space(user_input):
        print('ERROR: Leading whitespace is not supported.')
    elif detect_all_space(user_input):
        print("ERROR: All whitespace input not supported")
    elif detect_last_space(user_input):
        print("ERROR: Input cannot have trailing whitespace")
    else:
        valid_it += 1

        if valid_it == 1:
            string_output = user_input
        else:
            string_output = string_output + ' ' + user_input

print(f'\nThe summary of your input is as follows: {string_output}".\n')

input('\n\nPress any key to close Assignment_6_Jacocks.py:      ')
