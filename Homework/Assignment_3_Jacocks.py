def avg_func(a, b):
    ans = (a + b) / 2
    print(f"\nThe average of the two numbers {a} and {b} is :   {ans:.3f}")


inputA = float(input('\nPlease enter the first of two numbers to be averaged together:  '))
inputB = float(input('\nPlease enter the second of two numbers to be averaged together:  '))

avg_func(inputA, inputB)
